const { handlers } = require("../main");
const data = require("./data");

for (const datum of data) {
    if (!handlers[datum.object_kind]) {
        console.error("Unknown object: " + datum.object_kind);
        continue;
    }
    try {
        const embed = handlers[datum.object_kind](datum);
        if (embed) {
            console.log("Got embed for test case!", embed);
        } else {
            console.warn("No embed for test case?");
        }
    } catch (err) {
        console.error("Recieved error %o for case %o", err, datum);
    }
}
// I'm lazy, sorry
process.exit();
