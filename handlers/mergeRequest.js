const config = require("../config.json");

const actions = {
    open: (mergeRequest, projectName) =>
        `New merge request for ${projectName} - __${mergeRequest.title}__`,
    close: (mergeRequest, projectName) =>
        `Merge request for ${projectName} - __${mergeRequest.title}__ closed`,
    update: (mergeRequest, projectName) =>
        `Merge request for ${projectName} - __${mergeRequest.title}__ updated`,
    merge: (mergeRequest, projectName) =>
        `Merge request for ${projectName} - __${mergeRequest.title}__ merged`
};

module.exports = function(data) {
    const mergeRequest = data.object_attributes;

    return {
        title: (actions[mergeRequest.action] || actions.update)(
            mergeRequest,
            data.project.name
        ),
        url: mergeRequest.url,
        description:
            mergeRequest.action == "open"
                ? mergeRequest.description
                    ? mergeRequest.description
                    : "*No description provided*"
                : "",
        author: {
            name: `${data.user.name} (${data.user.username})`,
            icon_url: data.user.avatar_url,
            url: `${config.gitlabBase}/${data.user.username}`
        },
        color: 0xc080ff,
        timestamp: new Date().toISOString()
    };
};
